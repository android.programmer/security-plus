# Automation
- Continuous deployment is a software development method in which app and platform updates are commited to production rapidly.
- Continous delivery is a software development method in which app and platform requirements are frequently tested and validated for immediate availability. 
- Continuous integration is a software development method in which code updates are tested and committed to a development or build server/code repository rapidly.
- Continuous monitoring is the technique of constantly evaluating an environment for changes so that  new risks may be more quickly detected.